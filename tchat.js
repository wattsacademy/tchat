/**
 * Collections
 */
Messages = new Meteor.Collection('messages');

/**
 * Client Logic
 */
if (Meteor.isClient) {

    /**
     * Helpers
     */
    Template.messages.helpers({
        messages: function() {
            return Messages.find({}, {
                sort: {
                    time: -1
                }
            });
        }
    });

    /**
     * Events
     */

    Template.messageInsert.events({
        // message box event helper
        "keydown #message": function(e) {
            if (e.which == 13) {
                // Submit the form
                var name = document.getElementById('name');
                var message = document.getElementById('message');
                if (name.value != '' && message.value != '') {
                    Messages.insert({
                        name: name.value,
                        message: message.value,
                        time: Date.now()
                    });
                    Session.set('userName', name.value);
                    name.value = Session.get('userName');
                    message.value = '';
                }
            }
        },
        // send button event helper
        "click #send": function(e) {
            var name = document.getElementById('name');
            var message = document.getElementById('message');
            if (name.value != '' && message.value != '') {
                Messages.insert({
                    name: name.value,
                    message: message.value,
                    time: Date.now()
                });
                Session.set('userName', name.value);
                name.value = Session.get('userName');
                message.value = '';
            }
        }

    });

} // End isClient

/**
 * Server Logic
 */
if (Meteor.isServer) {}